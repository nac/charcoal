# minetest_charcoal Mod
This Mod add Charcoal to Minetest.  
**Features:**
- charcoal lump crafting from any item (group=tree)
- charcoal block crafting
- support black_dye crafting
- 4 torches

## Recipes
Charcoal Crafting:  
![Recipe for Charcoal](screenshots/2.png)  

Charcoal Block Crafting:
![Charcoal Block Crafting](screenshots/3.png)

## License
GNU LGPLv2+, [see LICENSE.txt](LICENSE.txt)  

